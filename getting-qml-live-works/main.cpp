#include <QGuiApplication>
#include <QQmlApplicationEngine>
#ifdef QT_DEBUG
#include "livenodeengine.h"
#include "remotereceiver.h"
#endif

int main(int argc, char *argv[])
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

    QGuiApplication app(argc, argv);
    QQmlApplicationEngine engine;

    QUrl qmlFile = QUrl(QStringLiteral("qrc:/main.qml"));

    engine.load(qmlFile);

    if (engine.rootObjects().isEmpty())
        return -1;

#ifdef QT_DEBUG
    LiveNodeEngine node;

    // Let QmlLive know your runtime
    node.setQmlEngine(&engine);

    // Allow it to display QML components with non-QQuickWindow root object
    QQuickView fallbackView(&engine, 0);
    node.setFallbackView(&fallbackView);

    // Tell it where file updates should be stored relative to
    node.setWorkspace(SRCDIR,
                      LiveNodeEngine::AllowUpdates | LiveNodeEngine::UpdatesAsOverlay);

    // Listen to IPC call from remote QmlLive Bench
    RemoteReceiver receiver;
    receiver.registerNode(&node);
    receiver.listen(10234);

    QQuickWindow* window = qobject_cast<QQuickWindow *>(engine.rootObjects().first());

    // Advanced use: let it know the initially loaded QML component (do this
    // only after registering to receiver!)
    QList<QQmlError> warnings;
    node.usePreloadedDocument(qmlFile.toString(), window, warnings);
#endif

    return app.exec();
}
