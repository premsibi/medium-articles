#ifndef STUDENT_H
#define STUDENT_H

#include <QObject>

class Student {
    Q_GADGET
    Q_PROPERTY(QString id MEMBER id)
    Q_PROPERTY(QString name MEMBER name)
    Q_PROPERTY(int year MEMBER year)

public:
    QString id;
    QString name;
    int year;
};

#endif // STUDENT_H
